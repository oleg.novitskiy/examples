package com.spduniversity.javacore.collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

public class DequeDemo {
    public static void main(String[] args) {
//        Deque<String> deque = new ArrayDeque<>();
        Deque<String> deque = new LinkedList<>();

        System.out.println("FIFO - Queue Model");
        deque.add("Item 1");
        deque.add("Item 2");
        deque.add("Item 3");
        deque.add("Item 4");
        System.out.println(deque);
        System.out.println(deque.remove());
        System.out.println(deque.remove());
        System.out.println(deque.remove());
        System.out.println(deque.remove());
        System.out.println(deque);
        System.out.println();

        System.out.println("LIFO - Stack Model");
        deque.push("Item 1");
        deque.push("Item 2");
        deque.push("Item 3");
        System.out.println(deque);
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        System.out.println(deque.poll());
        System.out.println(deque);
    }
}

package com.spduniversity.javacore.collections;

import java.util.*;

public class ArrayListDemo {
    public static void main(String[] args) {
//        listDemo();
//        iteratorDemo();
        listIteratorDemo();
    }

    private static void listIteratorDemo() {
        List<Integer> list = new ArrayList<>();
        list.addAll(Arrays.asList(1,2,3,4,5,6,7));
        System.out.println(list);

        System.out.println("ListIterator");
        ListIterator<Integer> iterator = list.listIterator();
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.previous());
    }

    private static void iteratorDemo() {
        List<Integer> list = new LinkedList<>();
        list.addAll(Arrays.asList(1,2,3,4,5,6,7));
        System.out.println(list);

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            int elem = iterator.next();
            System.out.println(elem);

            if (elem == 1) {
                iterator.remove();
            }
        }
        System.out.println(list);

        Integer[] numbers = new Integer[]{1, 2, 3, 4, 5};
        SimpleClass<Integer> myList = new SimpleClass<>(numbers);
        for (Integer elem : myList) {
            System.out.println(elem);
        }

        String[] strings = new String[] {"Hello", "World"};
        SimpleClass<String> myList1 = new SimpleClass<>(strings);
        for (String elem : myList1) {
            System.out.println(elem);
        }

        // Java 8 features
        list.forEach(System.out::println);
        System.out.println("Apply filter");
        list.forEach(Filter::filter);
        System.out.println("Consumer interface");
        list.forEach(new FilterWithConsumer<>());
    }

    private static void listDemo() {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        list.add(10);
        list.add(30);
        list.add(50);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        System.out.println(list);

        list.remove(1);
        System.out.println(list);
        list.remove(4);
        System.out.println(list);

        list.add(0, 100);
        System.out.println(list);
        int result = list.set(0, 99);
        System.out.println(list);
        System.out.println("result = " + result);
        System.out.println();

        System.out.println("Bulk operations");
        List<Integer> list1 = new ArrayList<>();
        list1.add(10);
        list1.add(99);
        list.removeAll(list1);
        System.out.println(list);

        list1.add(30);
        // Удаляет из коллекции list все элементы, которых нет в коллекции list1
        list.retainAll(list1);
        System.out.println(list);

        list.addAll(list1);
        System.out.println(list);

        System.out.println("Set(1, 55) and get value from 2 position: " + list1.set(1, 55));
        System.out.println(list1);
        System.out.println();

        System.out.println("Search operations");
        System.out.println("list.contains(99): " + list.contains(99));
        System.out.println("list.indexOf(99): " + list.indexOf(99));
        System.out.println("list.lastIndexOf(30): " + list.lastIndexOf(30));
        System.out.println(list);
        System.out.println();

        System.out.println("Iteration");
        for (int elem : list) {
            System.out.println(elem);
//            if (elem == 99) {
//                list.set(list.indexOf(elem), 1000);   // Correct
//            }
//             ConcurrentModificationException
//            if (elem == 99) {
//                list.remove(Integer.valueOf(elem));
//            }
        }
        System.out.println(list);
    }

    private static class FilterWithConsumer<T> implements java.util.function.Consumer<T> {
        @Override
        public void accept(T elem) {
            if (elem.equals(234325)) {
                System.out.println(elem);
            }
        }
    }
}

class SimpleClass<T> implements Iterable<T> {
    private T[] arrayList;
    private int currentSize;

    public SimpleClass(T[] arrayList) {
        this.arrayList = arrayList;
        this.currentSize = arrayList.length;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < currentSize && arrayList[index] != null;
            }

            @Override
            public T next() {
                return arrayList[index++];
            }
        };
    }
}

class Filter {
    static void filter(Integer i) {
        if (i == 5) {
            System.out.println(i);
        }
    }
}
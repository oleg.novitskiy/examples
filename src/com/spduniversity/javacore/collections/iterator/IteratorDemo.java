package com.spduniversity.javacore.collections.iterator;

import java.util.List;

public class IteratorDemo {
    public static void main(String[] args) {
        NameRepository namesRepository = new NameRepository();

        // Мы не знаем, внутренностей NameRepository
        for(Iterator iter = namesRepository.getIterator(); iter.hasNext();){
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }
    }

}

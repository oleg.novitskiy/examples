package com.spduniversity.javacore.concurrencyadv.synchronizers.countdown;

import java.util.concurrent.CountDownLatch;

class MyThread implements Runnable {
    private CountDownLatch latch;

    MyThread(CountDownLatch latch) {
        this.latch = latch;
        new Thread(this).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 2; i++) {
            System.out.println(i);
            latch.countDown();
        }
        System.out.println("Release self-blocking...");
    }
}

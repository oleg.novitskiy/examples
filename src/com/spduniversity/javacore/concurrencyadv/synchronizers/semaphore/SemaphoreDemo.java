package com.spduniversity.javacore.concurrencyadv.synchronizers.semaphore;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);

        new IncThread(semaphore, "A");
        new IncThread(semaphore, "A1");
        new DecThread(semaphore, "B");
        new DecThread(semaphore, "B1");
    }
}

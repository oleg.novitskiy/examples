package com.spduniversity.javacore.concurrencyadv.factorial;

import java.math.BigInteger;
import java.util.stream.IntStream;

public class FactorialDemo {
    public static final int NUMBER = 5;

    public static void main(String[] args) {
        System.out.println(NUMBER + "! = " + factorialParallel(NUMBER));
        System.out.println(NUMBER + "! = " + factorialBigIntParallel(NUMBER));
    }

    public static long factorialParallel(int n) {
        if (n < 2) return 1;
        return IntStream.rangeClosed(2, n).parallel()
                .reduce(1, (a, b) -> a * b);
    }

    public static BigInteger factorialBigIntParallel(int n) {
        if (n < 2) return BigInteger.ONE;
        return IntStream.rangeClosed(2, n).parallel()
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger::multiply).get();
    }
}

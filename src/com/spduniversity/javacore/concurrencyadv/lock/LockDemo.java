package com.spduniversity.javacore.concurrencyadv.lock;

import java.util.concurrent.locks.ReentrantLock;

public class LockDemo {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();

        new LockThread(lock, "A");
        new LockThread(lock, "B");
        new LockThread(lock, "C");
        new LockThread(lock, "D");
    }
}

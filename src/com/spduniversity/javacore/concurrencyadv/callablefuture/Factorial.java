package com.spduniversity.javacore.concurrencyadv.callablefuture;

import java.util.concurrent.Callable;

public class Factorial implements Callable<Integer> {
    private int number;

    Factorial(int num) {
        this.number = num;
    }

    @Override
    public Integer call() throws Exception {
        int result = 1;

        for (int i = 2; i <= number; i++) {
            result *= i;
        }

        return result;
    }
}

package com.spduniversity.javacore.concurrencyadv.executors;

import java.util.concurrent.CountDownLatch;

class MyThread implements Runnable {
    private final CountDownLatch latch;
    private final String name;

    public MyThread(CountDownLatch latch, String name) {
        this.latch = latch;
        this.name = name;
        new Thread(this);
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread " + name + " : " +i);
            latch.countDown();
        }
    }
}

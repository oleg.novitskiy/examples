package com.spduniversity.javacore.concurrencyadv.forkjoin.recursivetask;

public class RecursiveTaskDemo {
    private final static int POOL_SIZE = 10;

    public static void main(String[] args) {
        double[] nums = new double[POOL_SIZE];

        for (int i = 0; i < POOL_SIZE; i++) {
            nums[i] = (double) (i % 2 == 0 ? i : -i);
        }

        Sum task = new Sum(nums, 0, nums.length);

        double result = task.invoke();
        System.out.println("Sum: " + result);
    }
}

package com.spduniversity.javacore.concurrencyadv.homework;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Demo {

    public static void main(String[] args) {
        String urls[] = { "http://google.com", "http://apple.com"};
        copy(urls, Paths.get(""));
    }

    public static void copy(String[] urls, Path dst) {
        // TODO: Write your code here.

    }
    // копируем содержимое url в dst (и так тоже лучше не делать в продакшене)
    private static long download(String url, Path dst) {
        try {
            URI u = URI.create(url);
            try (InputStream in = u.toURL().openStream()) {
                Files.copy(in, dst, StandardCopyOption.REPLACE_EXISTING);
            }
            System.out.printf("%s -> %s %n", url, dst);
            return dst.toFile().length();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }
}

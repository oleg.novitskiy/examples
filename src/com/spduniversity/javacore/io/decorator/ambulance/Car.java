package com.spduniversity.javacore.io.decorator.ambulance;

class Car {
    protected String brandName;

    public void go() {
        System.out.printf("I'm " + brandName + " I'm go away!");
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}

package com.spduniversity.javacore.io.decorator.ambulance;

public class FlyingCar extends DecoratorCar {

    FlyingCar(Car decoratedCar) {
        super(decoratedCar);
    }

    @Override
    public void go() {
        super.go();
        System.out.println("Let's fly!");
    }
}

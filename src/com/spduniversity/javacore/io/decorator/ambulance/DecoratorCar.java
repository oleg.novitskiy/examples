package com.spduniversity.javacore.io.decorator.ambulance;

class DecoratorCar extends Car {
    private Car decoratedCar;

    DecoratorCar(Car decoratedCar) {
        this.decoratedCar = decoratedCar;
    }

    @Override
    public void go() {
        decoratedCar.go();
    }

    public Car getDecoratedCar() {
        return decoratedCar;
    }

    public void setDecoratedCar(Car decoratedCar) {

        this.decoratedCar = decoratedCar;
    }

    public void test() {
        System.out.println("Decorator test");
    }
}

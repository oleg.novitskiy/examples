package com.spduniversity.javacore.io.bytestream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BytesCopyDemo {
    public static void main(String[] args)  {

        try (FileInputStream in = new FileInputStream("bytecopydemo.txt");
             FileOutputStream out = new FileOutputStream("outagain.txt")) {

            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

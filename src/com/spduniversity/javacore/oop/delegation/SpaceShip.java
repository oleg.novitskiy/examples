package com.spduniversity.javacore.oop.delegation;

public class SpaceShip extends SpaceShipControls {
    private String name;

    public SpaceShip(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SpaceShip{" +
                "name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        SpaceShip protector = new SpaceShip("NASA Protector");
        protector.forward(100);
    }
}

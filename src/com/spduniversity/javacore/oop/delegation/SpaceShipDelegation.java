package com.spduniversity.javacore.oop.delegation;

public class SpaceShipDelegation {
    private String name;
    private SpaceShipControls controls = new SpaceShipControls();

    SpaceShipDelegation(String name) {
        this.name = name;
    }

    void up(int velocity) {
        controls.up(velocity);
    }

    void down(int velocity) {
        controls.down(velocity);
    }

    void left(int velocity) {
        controls.left(velocity);
    }

    void right(int velocity) {
        controls.right(velocity);
    }

    void forward(int velocity) {
        controls.forward(velocity);
    }

    @Override
    public String toString() {
        return "SpaceShipDelegation{" +
                "name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        SpaceShipDelegation protector = new SpaceShipDelegation("NASA Protector");
        protector.forward(100);
    }
}

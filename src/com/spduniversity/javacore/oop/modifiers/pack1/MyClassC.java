package com.spduniversity.javacore.oop.modifiers.pack1;

public class MyClassC extends MyClassA {

    public static void main(String[] args) {
        // MyClassA.privateField is not inherited!!!

        System.out.println("defaultField: " + defaultField);
        System.out.println("protectedField: " + protectedField);
        System.out.println("publicField: " + publicField);
    }
}

package com.spduniversity.javacore.oop.modifiers.pack1;

import com.spduniversity.javacore.oop.modifiers.pack2.MyClassAnotherC;

public class MyClassB {
    public static void main(String[] args) {
//        System.out.println("MyClassA.privateField: " + MyClassA.privateField);

        System.out.println(MyClassA.defaultField);
        System.out.println(MyClassA.protectedField);
        System.out.println(MyClassA.publicField);

        System.out.println(MyClassC.defaultField);
        System.out.println(MyClassC.protectedField);
        System.out.println(MyClassC.publicField);

        System.out.println(MyClassAnotherC.protectedField);
        System.out.println(MyClassAnotherC.publicField);
    }
}

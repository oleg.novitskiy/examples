package com.spduniversity.javacore.oop.modifiers.pack2;

import com.spduniversity.javacore.oop.modifiers.pack1.MyClassA;
import com.spduniversity.javacore.oop.modifiers.pack1.MyClassC;

public class MyClassAnotherC extends MyClassA {

    public static void main(String[] args) {
        System.out.println(protectedField);
        System.out.println(publicField);

        System.out.println(MyClassC.protectedField);
        System.out.println(MyClassC.publicField);
    }
}

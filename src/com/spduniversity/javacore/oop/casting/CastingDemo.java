package com.spduniversity.javacore.oop.casting;

import java.util.ArrayList;

class Instrument {
    void play() {
        System.out.println("Instrument plays");
    }
}

class Flute extends Instrument {
    @Override
    void play() {
        System.out.println("Flute plays");
    }
}

class Drum extends Instrument {
    @Override
    void play() {
        System.out.println("Drum beats");
    }
}

public class CastingDemo {
    public static void main(String[] args) {
        ArrayList<Instrument> instruments = new ArrayList<>();
        instruments.add(new Flute());
        instruments.add(new Drum());
        instruments.add(new Instrument());

        for (Instrument instrument: instruments) {
            instrument.play();
        }
    }
}

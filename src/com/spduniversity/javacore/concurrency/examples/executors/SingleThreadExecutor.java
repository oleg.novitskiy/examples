package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadExecutor {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Runnable task1 = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Task1 " + threadName);
        };
        Runnable task2 = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Task2 " + threadName);
        };
        Runnable task3 = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Task3 " + threadName);
        };

        executor.submit(task1);
        executor.submit(task2);
        executor.submit(task3);
    }
}

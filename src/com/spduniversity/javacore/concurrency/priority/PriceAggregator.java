package com.spduniversity.javacore.concurrency.priority;

public class PriceAggregator implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Price aggregator task.");
            if (i % 2 == 0) {
                Thread.yield();
            }
        }
    }
}

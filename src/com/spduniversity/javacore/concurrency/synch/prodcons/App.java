package com.spduniversity.javacore.concurrency.synch.prodcons;

public class App {
    public static void main(String[] args) {
        Q q = new QFixed();
//        Q q = new QWrong();
        new Producer(q);
        new Consumer(q);

        System.out.println("Exit from main()...");
    }

}

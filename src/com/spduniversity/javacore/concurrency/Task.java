package com.spduniversity.javacore.concurrency;

class Task implements Runnable {
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println("Task.run()... " + threadName);
        methodInsideRun();
    }

    private void methodInsideRun() {
        System.out.println("Inside run()...");
        anotherDeepMethod();
    }

    private void anotherDeepMethod() {
        System.out.println("Inside anotherDeepMethod()...");
    }
}

package com.spduniversity.javacore.staticfinal;

public class StaticVarExample {
    int instanceVariable;
    static int staticVariable;

    void method() {
        instanceVariable++;
        staticVariable++;
        staticMethod();
    }

    public static void staticMethod() {
        staticVariable++;
//        instanceVariable++;    // Error!
//        method();      // Error!
        (new StaticVarExample()).method();  // It works
    }
}

package com.spduniversity.javacore.staticfinal;

import java.text.ParseException;
import java.util.HashMap;

public class StaticInitializerDemo {
    static HashMap<String, Float> employee = new HashMap<>();
    private int age;

    static {
        employee.put("Billy", 1000.0f);
        employee.put("Billy", 2000.0f);
        employee.put("Billy1", 1000.0f);
        employee.put("Billy2", 1000.0f);
        employee.put("Billy3", 1000.0f);
        employee.put("Billy4", 1000.0f);
        employee.put("Billy5", 1000.0f);
//        age = 100;    // Compile Error!
//        method1();    // Compile Error!
    }

    private void method1() {}

    static {
        employee.put("Jacky", 2000.f);
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(StaticInitializerDemo.employee);
    }
}

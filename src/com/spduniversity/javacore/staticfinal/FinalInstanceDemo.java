package com.spduniversity.javacore.staticfinal;

/**
 *
 */
public class FinalInstanceDemo {
    private final int id = 10;
    private final int age;
    private final int weight;

    public static final int ALL_CONSTANT_SHOULD_BE_LIKE_THAT = 10;

    {
        age = 25;
    }

    FinalInstanceDemo() {
        weight = 100;
//        id = 1000;
    }

    public void accelerate(final int delta) {
//        delta++;    // Compile Error!
    }

    public void testMethod() {
        final int someVariable = 10;

//        someVariable = 20;  // Compile Error!

    }

    public void testMethod1(int speed) {
        final int mySpeed = 200;
//        mySpeed = 200;

        switch (speed) {
            case 120:
                System.out.println("Cool");
                break;
            case mySpeed:
                System.out.println("My speed");
                break;
        }
    }
}

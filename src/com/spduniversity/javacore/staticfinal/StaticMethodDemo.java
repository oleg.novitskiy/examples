package com.spduniversity.javacore.staticfinal;

public class StaticMethodDemo {
    public static void main(String[] args) {
        Car car = new Car("");
        car = null;
        Car.accelerate();
    }
}

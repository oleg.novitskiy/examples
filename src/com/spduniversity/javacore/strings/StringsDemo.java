package com.spduniversity.javacore.strings;

import java.io.IOException;

class StringsDemo {

    public static void main(String[] args) throws IOException {
        String msg = "Ahoy, there!";
        System.out.println("Msg: " + msg);

        System.out.println("\nmsg.length(): " + msg.length());
        System.out.println("msg.isEmpty(): " + msg.isEmpty());

        // 1. Сравнение строк
        System.out.println("\nmsg.equals(\"AHOY, THERE!\"): " +
                msg.equals("AHOY, THERE!"));
        System.out.println("msg.equalsIgnoreCase(\"AHOY, THERE!\"): " +
                msg.equalsIgnoreCase("AHOY, THERE!"));
        System.out.println("msg.compareTo(\"AHOY, THERE!\"): " +
                msg.compareTo("AHOY, THERE!"));

        // 2. Searching
        System.out.println("\nmsg.contains(\"AHOY, THERE!\"): " +
                msg.contains("AHOY, THERE!"));
        System.out.println("msg.contains(\"there!\"): " + msg.contains("world!"));
        System.out.println("msg.startsWith(\"AHOY, THERE!\"): " +
                msg.startsWith("AHOY, THERE!"));
        System.out.println("msg.startsWith(\"ahoy, there!\"): " +
                msg.startsWith("ahoy, there!"));
        System.out.println("msg.endsWith(\"!\"): " + msg.endsWith("!"));
        System.out.println("msg.indexOf(\"th\"): " + msg.indexOf("th"));
        System.out.println("msg.indexOf(\"o\"): " + msg.indexOf('o'));
        System.out.println("msg.lastIndexOf(\"e\"): " + msg.lastIndexOf('e'));

        // 3. Examining individual characters
        System.out.println("\nmsg.charAt(4): " + msg.charAt(4));

        // 3. Extracting substrings
        System.out.println("\nmsg.substring(3): " + msg.substring(4));
        System.out.println("msg.substring(3, 10): " + msg.substring(4, 9));

        // 4. Case conversions
        System.out.println("\nmsg.toUpperCase(): " + msg.toUpperCase());
        System.out.println("msg.toLowerCase(): " + msg.toLowerCase());

        System.out.println("\nmsg.trim(): " + msg.trim()); // returns a copy of string after trimming any leading & trailing white spaces

        // 5. Replace
        System.out.println("\nmsg.replaceAll(\"o\", \"r\"): " +
                msg.replaceAll("o", "r"));

        // 6. Split
        System.out.println("\nmsg.split(\"o\"): ");
        String[] os = msg.split("o");
        for (String tmp : os) {
            System.out.println(tmp);
        }

        System.out.println("\nmsg.valueOf(3.1415): " + String.valueOf(3.1415));

    }
}

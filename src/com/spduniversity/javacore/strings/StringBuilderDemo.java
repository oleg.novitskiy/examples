package com.spduniversity.javacore.strings;

public class StringBuilderDemo {

    public static void main(String[] args) {
        StringBuffer builder = new StringBuffer();

        builder = builder.append("SELECT * ")
                .append("FROM Users ")
                .append("WHERE ")
                .append("id = ")
                .append(5);
        System.out.println(builder.toString());
    }
}

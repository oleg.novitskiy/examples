package com.spduniversity.javacore.strings;

import java.util.Arrays;

public class RegularExpressionDemo {
    private static String knights =
            "Then, when you have found the shrubbery, you must" +
                    "cut down the mightiest tree in the forest... " +
                    "with... a herring!";

    public static void main(String[] args) {
        System.out.println(Arrays.toString(knights.split(" ")));
        System.out.println(Arrays.toString(knights.split("\\W+")));
        System.out.println("\n" + knights.replaceFirst("f\\w+", "located"));
        System.out.println(knights.replaceAll("shrubbery|tree|herring", "banana"));
        System.out.println("Rudolph".matches("[rR][aeiou][a-z]ol.*"));
    }
}

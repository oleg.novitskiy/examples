package com.spduniversity.javacore.reflection.simple;

public class Student {
    public String name;
    public int age;

    public void setName(String name) {
        this.name = name;
    }
}

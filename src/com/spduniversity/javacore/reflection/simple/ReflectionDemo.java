package com.spduniversity.javacore.reflection.simple;

import java.lang.reflect.Field;

public class ReflectionDemo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Student student = new Student();
        student.setName("alex");

        Class c = student.getClass();
        Field field = c.getField("name");
        // Получить значение поля
        System.out.println("Student.name: " + (String) field.get(student)); // alex

        // Установить значение поля
        field.set(student, "bob");
        System.out.println("Student.name: " + (String) field.get(student)); // bob

    }
}

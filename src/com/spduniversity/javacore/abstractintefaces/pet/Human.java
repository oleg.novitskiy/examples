package com.spduniversity.javacore.abstractintefaces.pet;

public class Human implements Animal {
	private String name;

	public String getName() {
		return name;
	}
	
	public Human(String name) {
		this.name = name;
	}



	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void voice() {
		System.out.println("I am a human. My name is " + name );
	}
}

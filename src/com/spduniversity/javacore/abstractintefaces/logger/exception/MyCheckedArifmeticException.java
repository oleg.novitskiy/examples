package com.spduniversity.javacore.abstractintefaces.logger.exception;

public class MyCheckedArifmeticException extends MyCheckedException {
	int a;
	int b;
	
	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}

	public MyCheckedArifmeticException() {
		super();
	}
	
	String message;
	public void setMessage(String message){
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
	
	
}

package com.spduniversity.javacore.annotations.jobtitle;

public @interface History {
    Version[] value() default {};
}

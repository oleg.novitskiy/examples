package com.spduniversity.javacore.annotations.jobtitle;

import static com.spduniversity.javacore.annotations.jobtitle.JobTitle.JUNIOR;
import static com.spduniversity.javacore.annotations.jobtitle.JobTitle.MIDDLE;
import static com.spduniversity.javacore.annotations.jobtitle.JobTitle.SENIOR;

@History({
    @Version(
            version = 1,
            date = @Date(year = 2000, month = 1, day = 1)),
    @Version(
            version = 2,
            date = @Date(year = 2003, month = 3, day = 10),
            authors = {@Author(value = "Jim Smith", title = MIDDLE)},
            previous = MyClassVer1.class),
    @Version(
            version = 3,
            date = @Date(year = 2010, month = 11, day=15),
            authors = {
                    @Author(value="Bill Gates", title = JUNIOR),
                    @Author(value="Tim Lee", title = SENIOR)
            },
            previous = MyClassVer2.class)
})
public class MyClassVer3 {}


package com.spduniversity.javacore.annotations.jobtitle;

public @interface Date {
    int day();
    int month();
    int year();
}

package com.spduniversity.javacore.generics.simple;

public class Pair<T1, T2> {
    T1 object1;
    T2 object2;

    public Pair(T1 object1, T2 object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    public T1 getObject1() {
        return object1;
    }

    public T2 getObject2() {
        return object2;
    }

    public static void main(String[] args) {

        Pair<Integer, String> pair = new Pair<Integer, String>(1, " April");

        Pair<Integer, String> pair2 = new Pair<>(1, " April");


        System.out.println(pair.getObject1() + pair.getObject2());
    }
}

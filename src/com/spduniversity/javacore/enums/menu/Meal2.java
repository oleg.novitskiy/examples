package com.spduniversity.javacore.enums.menu;

import java.util.Arrays;

/**
 * Интерфейсы, вложенные в enums
 */
public enum Meal2 {
    Appetizer(Food.Appetizer.class),
    MainCourse(Food.MainCourse.class),
    Dessert(Food.Dessert.class),
    Coffee(Food.Coffee.class);

    private Food[] values;

    // private constructor!
    Meal2(Class<? extends Food> kind) {
        values = kind.getEnumConstants();
    }

    @Override
    public String toString() {
        return "Meal2{" +
                "values=" + Arrays.toString(values) +
                '}';
    }

    public interface Food {
        enum Appetizer implements Food {
            Salad, Soup, SprintingRolls;
        }

        enum MainCourse implements Food {
            Lasagne, Buritto, PadThai, Lentils;
        }

        enum Dessert implements Food {
            Fruit, Tiramisu, BlackForestCake;
        }

        enum Coffee implements Food {
            BlackCoffe, Espresso, Latte;
        }
    }

    public static void main(String[] args) {
        for (Meal2 meal: Meal2.values()) {
            System.out.println(meal);
        }
    }
}

package com.spduniversity.javacore.enums.impl;

public interface Printable {
    void print();
}

package com.spduniversity.javacore.enums.impl;

public class App {
    public static void main(String[] args) {
        Apple apple;

        apple = Apple.Golden;
        apple.print();
        apple.log();
    }
}

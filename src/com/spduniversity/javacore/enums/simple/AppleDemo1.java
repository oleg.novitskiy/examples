package com.spduniversity.javacore.enums.simple;

public class AppleDemo1 {
    public static void main(String[] args) {
        Apple apple;

        apple = Apple.RedDel;
        System.out.println("Значение apple: " + apple);

        apple = Apple.GoldenDel;
        if (apple == Apple.GoldenDel) {
            System.out.println("Переменная apple содержить значение GoldenDel");
        }

        switch (apple) {
            case Jonathan:
                System.out.println("Сорт Jonathan красный.");
                break;
            case GoldenDel:
                System.out.println("Сорт Golden Delicious желтый.");
                break;
            case RedDel:
                System.out.println("Сорт Red Delicious красный.");
                break;
            case Winesap:
                System.out.println("Сорт Winesap красный.");
                break;
            case Cortland:
                System.out.println("Сорт Cortland красный.");
                break;
        }
    }
}

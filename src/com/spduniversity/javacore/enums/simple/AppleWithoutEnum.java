package com.spduniversity.javacore.enums.simple;

final class AppleWithoutEnum {
    static final int JONATHAN = 1;
    static final int GOLDENDEL = 2;
    static final int REDDEL = 3;
    static final int WINESAP = 4;
    static final int CORTLAND = 5;
}

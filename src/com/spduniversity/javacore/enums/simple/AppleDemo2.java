package com.spduniversity.javacore.enums.simple;

public class AppleDemo2 {
    public static void main(String[] args) {
        Apple apple;
        System.out.println("Константы типа Apple:");

        Apple apples[] = Apple.values();
        for (Apple a: apples) {
            System.out.println(a);
        }

        Apple newApple = Apple.valueOf("Winesapasdf");
        System.out.println("Переменная newApple содержит " + newApple);
    }
}

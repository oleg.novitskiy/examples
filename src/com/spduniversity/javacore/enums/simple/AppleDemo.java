package com.spduniversity.javacore.enums.simple;

public class AppleDemo {
    public static void main(String[] args) {
        printAppleType(Apple.Winesap);
    }

    private static void printAppleType(Apple apple) {
        System.out.println(apple);
    }

    private static void printAppleTypeWithoutEnum(int type) {
        switch (type) {
            case AppleWithoutEnum.JONATHAN:
                System.out.println(AppleWithoutEnum.JONATHAN);
                break;
            case AppleWithoutEnum.GOLDENDEL:
                System.out.println(AppleWithoutEnum.GOLDENDEL);
                break;
            case AppleWithoutEnum.REDDEL:
                System.out.println(AppleWithoutEnum.REDDEL);
                break;
            case AppleWithoutEnum.WINESAP:
                System.out.println(AppleWithoutEnum.WINESAP);
                break;
            case AppleWithoutEnum.CORTLAND:
                System.out.println(AppleWithoutEnum.CORTLAND);
                break;
        }
    }
}

// Нельзя наследовать от enum
//class MyApple extends Apple {
//
//}
